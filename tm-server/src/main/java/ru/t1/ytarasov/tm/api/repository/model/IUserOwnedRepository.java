package ru.t1.ytarasov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void add(@NotNull final String userId, @NotNull final M model);

    @Nullable
    List<M> findAll(@NotNull final String userId);

    @Nullable
    M findOneById(@NotNull final String userId, @NotNull final String id);

    int getSize(@NotNull final String userId);

    boolean existsById(@NotNull final String userId, @NotNull final String id);

}
