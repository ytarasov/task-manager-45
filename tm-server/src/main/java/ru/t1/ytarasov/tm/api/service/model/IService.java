package ru.t1.ytarasov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @Nullable
    List<M> findAll() throws Exception;

    int getSize() throws Exception;

    @NotNull
    M add(@Nullable final M model) throws Exception;

    @Nullable
    Collection<M> add(@Nullable final Collection<M> models) throws Exception;

    boolean existsById(@Nullable final String id) throws Exception;

    @Nullable
    M findOneById(@Nullable final String id) throws Exception;

    void clear() throws Exception;

    @Nullable
    M remove(@Nullable final M model) throws Exception;

    @Nullable
    M removeById(@Nullable final String id) throws Exception;

    void update(@Nullable final M model) throws Exception;

}
