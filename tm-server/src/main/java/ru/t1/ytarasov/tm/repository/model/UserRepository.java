package ru.t1.ytarasov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.EntityConstant;
import ru.t1.ytarasov.tm.api.repository.model.IUserRepository;
import ru.t1.ytarasov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository
        extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<User> findAll() {
        @NotNull final String jpql = String.format("FROM %s", getTableName());
        return entityManager
                .createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT u FROM %s u WHERE u.%s = :id",
                getTableName(), EntityConstant.COLUMN_ID);
        return entityManager
                .createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final String jpql = String.format("SELECT u FROM %s u WHERE u.%s = :login",
                getTableName(), EntityConstant.COLUMN_LOGIN);
        return entityManager
                .createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    public @Nullable User findByEmail(@NotNull final String email) {
        @NotNull final String jpql = String.format("SELECT u FROM %s u WHERE u.%s = :email",
                getTableName(), EntityConstant.COLUMN_EMAIL);
        return entityManager
                .createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        @Nullable final User user = this.findByLogin(login);
        if (user == null) return;
        remove(user);
    }

    @Override
    protected @NotNull String getTableName() {
        return EntityConstant.TABLE_USER;
    }

}
