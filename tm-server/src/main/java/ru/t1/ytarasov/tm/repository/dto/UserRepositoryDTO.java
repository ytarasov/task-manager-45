package ru.t1.ytarasov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.DBConstant;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.IUserRepositoryDTO;
import ru.t1.ytarasov.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepositoryDTO
        extends AbstractRepositoryDTO<UserDTO> implements IUserRepositoryDTO {

    public UserRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() {
        @NotNull final String jpql = String.format("FROM %s", getTableName());
        return entityManager
                .createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    public @Nullable UserDTO findOneById(@NotNull String id) {
        @NotNull final String jpql = String.format("SELECT s FROM %s s WHERE s.%s = :id",
                getTableName(), EntityConstantDTO.COLUMN_ID);
        return entityManager
                .createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    public @Nullable UserDTO findByLogin(@NotNull String login) {
        @NotNull final String jpql = String.format("SELECT s FROM %s s WHERE s.%s = :login",
                getTableName(), EntityConstantDTO.COLUMN_LOGIN);
        return entityManager
                .createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    public @Nullable UserDTO findByEmail(@NotNull String email) {
        @NotNull final String jpql = String.format("SELECT s FROM %s s WHERE s.%s = :email",
                getTableName(), EntityConstantDTO.COLUMN_EMAIL);
        return entityManager
                .createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    public void removeByLogin(@NotNull String login) {
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return;
        remove(user);
    }

    @Override
    protected @NotNull String getTableName() {
        return EntityConstantDTO.TABLE_USER;
    }

}
