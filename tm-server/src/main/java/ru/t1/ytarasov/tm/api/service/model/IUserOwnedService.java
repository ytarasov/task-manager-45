package ru.t1.ytarasov.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    void clear(@Nullable final String userId) throws Exception;

    boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception;

    @Nullable
    List<M> findAll(@Nullable final String userId) throws Exception;

    @Nullable
    M findOneById(@Nullable final String userId, @Nullable final String id) throws Exception;

    int getSize(@Nullable final String userId) throws Exception;

    @Nullable
    M removeById(@Nullable final String userId, @Nullable final String id) throws Exception;

    @Nullable
    M add(@Nullable final String userId, @Nullable final M model) throws Exception;

    @Nullable
    M remove(@Nullable final String userId, @Nullable final M model) throws Exception;

}
