package ru.t1.ytarasov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.DBConstant;
import ru.t1.ytarasov.tm.api.EntityConstant;
import ru.t1.ytarasov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.ytarasov.tm.model.AbstractUserOwnedModel;
import ru.t1.ytarasov.tm.model.User;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        @NotNull final String jpql = String.format("FROM %s WHERE %s = :id",
                DBConstant.TABLE_USER, EntityConstant.COLUMN_ID);
        @NotNull final User user = entityManager.createQuery(jpql, User.class)
                .setParameter("id", userId)
                .getSingleResult();
        model.setUser(user);
        add(model);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final String jpql = String.format("SELECT COUNT(m) FROM %s m WHERE m.%s = :userId",
                getTableName(), EntityConstant.COLUMN_USER_ID);

        return entityManager
                .createQuery(jpql, int.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT COUNT(1) = 1 FROM %s m WHERE %s = :id AND m.%s = :userId",
                getTableName(), EntityConstant.COLUMN_ID, EntityConstant.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

}
