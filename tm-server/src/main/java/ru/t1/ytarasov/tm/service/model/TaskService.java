package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.model.ITaskRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.model.ITaskService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.ModelNotFoundException;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.repository.model.TaskRepository;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class TaskService
        extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Nullable
    @Override
    public List<Task> findAll() throws Exception {
        @Nullable List<Task> tasks;
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            tasks = repository.findAll();
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override
    public int getSize() throws Exception {
        int size;
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            size = repository.getSize();
        } finally {
            entityManager.close();
        }
        return size;
    }

    @NotNull
    @Override
    public Task add(@Nullable Task model) throws Exception {
        if (model == null) throw new TaskNotFoundException();
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    public Collection<Task> add(@Nullable Collection<Task> models) throws Exception {
        if (models == null) throw new TaskNotFoundException();
        for (@NotNull final Task model : models) add(model);
        return models;
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsById;
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            existsById = repository.existsById(id);
        } finally {
            entityManager.close();
        }
        return existsById;
    }

    @Override
    public @Nullable Task findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Task task;
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            task = repository.findOneById(id);
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public void clear() throws Exception {
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
    @Nullable
    @Override
    public Task remove(@Nullable Task model) throws Exception {
        if (model == null) throw new TaskNotFoundException();
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public @Nullable Task removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

    @Override
    public void update(@Nullable Task model) throws Exception {
        if (model == null) throw new TaskNotFoundException();
        model.setUpdated(new Date());
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        @Nullable List<Task> tasks;
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            tasks = repository.findAll(comparator);
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable Sort sort) throws Exception {
        if (sort == null) return findAll();
        @Nullable List<Task> tasks;
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            tasks = repository.findAll(sort.getComparator());
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override
    public @Nullable List<Task> findAll(@Nullable final String userId, @Nullable final Comparator comparator) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        @Nullable List<Task> tasks;
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            tasks = repository.findAll(userId, comparator);
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override
    public @Nullable List<Task> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll();
        @Nullable List<Task> tasks;
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            tasks = repository.findAll(userId, sort.getComparator());
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> findByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable List<Task> tasks;
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            tasks = repository.findByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Nullable
    @Override
    public Task create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public Task changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @Override
    public void clear(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Task> tasks = findAll(userId);
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        for (@NotNull final Task task : tasks) remove(userId, task);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsById;
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            existsById = repository.existsById(id);
        } finally {
            entityManager.close();
        }
        return existsById;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable List<Task> tasks;
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            tasks = repository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Task task;
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            task = repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public int getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        int size;
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            size = repository.getSize(userId);
        } finally {
            entityManager.close();
        }
        return size;
    }

    @Nullable
    @Override
    public Task removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

    @Override
    public @Nullable Task add(@Nullable String userId, @Nullable Task model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return null;
    }

    @Override
    public @Nullable Task remove(@Nullable String userId, @Nullable Task model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @Nullable final Task task = findOneById(userId, model.getId());
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

}
