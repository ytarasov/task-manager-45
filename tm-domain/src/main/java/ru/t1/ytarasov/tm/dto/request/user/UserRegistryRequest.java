package ru.t1.ytarasov.tm.dto.request.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.AbstractRequest;
import ru.t1.ytarasov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRegistryRequest extends AbstractRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

    @Nullable
    private Role role;

}
