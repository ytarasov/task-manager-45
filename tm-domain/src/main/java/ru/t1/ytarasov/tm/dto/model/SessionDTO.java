package ru.t1.ytarasov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SessionDTO extends AbstractUserOwnedModelDTO {

    private static final long serialVersionUID = 1;

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role;

}
