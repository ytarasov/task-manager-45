package ru.t1.ytarasov.tm.exception.user;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! This e-mail already exists...");
    }
}
