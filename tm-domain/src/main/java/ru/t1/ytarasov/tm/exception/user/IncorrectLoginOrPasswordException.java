package ru.t1.ytarasov.tm.exception.user;

public final class IncorrectLoginOrPasswordException extends AbstractUserException {

    public IncorrectLoginOrPasswordException() {
        super("Error! Login or password are incorrect...");
    }

}
