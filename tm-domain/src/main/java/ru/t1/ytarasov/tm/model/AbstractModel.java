package ru.t1.ytarasov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractModel {

    @Id
    @NotNull
    protected String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    protected Date created = new Date();

    @Column
    @NotNull
    protected Date updated = new Date();

}
