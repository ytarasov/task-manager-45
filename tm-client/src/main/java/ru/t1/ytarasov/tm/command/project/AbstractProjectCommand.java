package ru.t1.ytarasov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ytarasov.tm.command.AbstractCommand;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return getServiceLocator().getProjectEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role @Nullable [] getRoles() {
        return Role.values();
    }

    public void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId() + ";");
        System.out.println("Name: " + project.getName() + ";");
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.toName(project.getStatus()));
        System.out.println("Created: " + project.getCreated());
    }

}
